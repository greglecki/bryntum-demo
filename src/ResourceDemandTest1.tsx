import React, { useEffect, useRef } from "react";
import { ProjectModel } from "@bryntum/gantt";
import { CalendarModel } from "@bryntum/schedulerpro";
import {
  BryntumSchedulerPro,
  BryntumResourceHistogram,
  BryntumSplitter,
} from "@bryntum/schedulerpro-react";
// Error trying to get it from schedulerpro.umd.js: Module '"@bryntum/schedulerpro/schedulerpro.umd.js"' has no exported member 'CalculatedValueGen'.
import { CalculatedValueGen } from "@bryntum/gantt/source/lib/ChronoGraph/chrono/Identifier";
// Error trying to get it from schedulerpro.umd.js: Module '"@bryntum/schedulerpro/schedulerpro.umd.js"' has no exported member 'ResourceAllocationInfo'.
import { ResourceAllocationInfo } from "@bryntum/gantt/source/lib/Engine/quark/model/scheduler_pro/SchedulerProResourceMixin";
import "@bryntum/gantt/gantt.material.css";

const data = [
  {
    id: 1,
    startDate: "2022-10-01",
    endDate: "2022-10-05",
    name: "Conference",
  },
  {
    id: 2,
    startDate: "2022-10-06",
    endDate: "2022-10-10",
    name: "Prototype",
  },
  {
    id: 3,
    startDate: "2022-10-15",
    endDate: "2022-10-18",
    name: "Test",
  },
];
const resources = [
  {
    id: "r1",
    name: "Celia",
    city: "Barcelona",
  },
  {
    id: "r2",
    name: "Lee",
    city: "London",
  },
];
const assignments = [
  {
    id: 1,
    resource: "r1",
    event: 1,
  },
  {
    id: 2,
    resource: "r2",
    event: 2,
  },
  {
    id: 3,
    resource: "r1",
    event: 3,
  },
];

function ResourceDemandTest1() {
  const schedulerRef = useRef<any>();
  const histogramRef = useRef<any>();

  // setup partnership between scheduler and histogram
  useEffect(() => {
    histogramRef.current?.instance.addPartner(schedulerRef.current?.instance);
  }, []);

  const project = new ProjectModel({
    resources,
    assignments,
    eventsData: data,
  });

  const buttonClicked = () => {
    const resource = project.resourceStore.first;
    const graph = (project as any).getGraph();
    const ticksIdentifier = graph.addIdentifier(CalculatedValueGen.new());
    const allocationReport = ResourceAllocationInfo.new({
      resource,
      ticks: ticksIdentifier,
    } as any);

    // register ResourceAllocationInfo instance in the graph
    graph.addEntity(allocationReport);

    // write value to ticksIdentifier
    ticksIdentifier.writeToGraph(
      graph,
      new CalendarModel({
        unspecifiedTimeIsWorking: false,
        intervals: [
          {
            startDate: new Date(2022, 10, 1),
            endDate: new Date(2022, 10, 5),
            isWorking: true,
          } as any,
          {
            startDate: new Date(2022, 10, 6),
            endDate: new Date(2022, 10, 10),
            isWorking: true,
          },
          {
            startDate: new Date(2022, 10, 15),
            endDate: new Date(2022, 10, 18),
            isWorking: true,
          },
        ],
      })
    );
    (resource as any).entities.add(allocationReport);
    project.commitAsync().then((data) => {
      console.log("Allocation Report", allocationReport.allocation);
    });
  };

  return (
    <>
      <button onClick={() => buttonClicked()}>Test</button>
      <BryntumSchedulerPro
        ref={schedulerRef}
        columns={[
          { type: "resourceInfo", text: "Name", field: "name", width: 130 },
          { text: "City", field: "city", width: 90 },
        ]}
        project={project}
      />
      <BryntumSplitter />
      <BryntumResourceHistogram
        ref={histogramRef}
        project={project}
        columns={[
          {
            type: "resourceInfo",
            text: "Name",
            field: "name",
            flex: 1,
            showEventCount: true,
          },
        ]}
      />
    </>
  );
}

export default ResourceDemandTest1;
